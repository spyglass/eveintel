package eveintel

import (
	"encoding/json"
	"net/http"
	"github.com/rs/cors"
	"strings"
	"log"
	"fmt"
)

var (
	addr     = ":13271"
	compress = false
)

func (im IntelEngine) Server() () {

	mux := http.NewServeMux()

	mux.HandleFunc("/list", im.serveJSONIntel)
	mux.HandleFunc("/idb", im.serveIntelDB)
	mux.HandleFunc("/latest", im.serveLatestIntel)
	mux.HandleFunc("/changeregion/", im.serveRegionChange)
	mux.HandleFunc("/currentregion", im.serveCurrentRegion)
	

	handler := cors.AllowAll().Handler(mux)
	
	if err := http.ListenAndServe(addr, handler); err != nil {
		panic(err)
	}
}

func (im *IntelEngine) serveJSONIntel(w http.ResponseWriter, r *http.Request) () {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(im.SystemMap)
}

func (im *IntelEngine) serveCurrentRegion(w http.ResponseWriter, r *http.Request) () {

	//w.Header().Set("Content-Type", "application/text")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, im.currentRegion)
}

func (im *IntelEngine) serveIntelDB(w http.ResponseWriter, r *http.Request) () {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(im.idb.messages)

}

func (im *IntelEngine) serveLatestIntel(w http.ResponseWriter, r *http.Request) () {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(im.idb.GetLatestFilter())
}

func (im *IntelEngine) serveRegionChange(w http.ResponseWriter, r *http.Request) () {
	region := strings.TrimPrefix(r.URL.Path, "/changeregion/")

	valids := getSystemList()

	for _, valid := range valids {
		if region == valid {
			im.initIntelRegion(region)

			log.Println("Updated map list to " + region)
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("Valid Region Change"))
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("Invalid Region"))
}