package eveintel

import (
	"fmt"
	"gitlab.com/spyglass/evemap"
	"gitlab.com/spyglass/spyglasscfg"
	"sort"
	"testing"
)

func TestParseStatus(t *testing.T) {
}

func TestParseSystems(t *testing.T) {
	tests := []struct {
		message   string
		fullnames string
	}{
		{"S25C-K", "[S25C-K]"},
		{"DP-JD", "[DP-JD4]"},
		{"9UY", "[9UY4-H]"},
		{"J6", "[J6QB-P]"},
		{"UQ-", "[UQ-PWD]"},
		{"U-Q", "[U-QVWD]"},
		{"UQ", "[UQ-PWD]"},

		{"S25C-K full of reds", "[S25C-K]"},
		{"DP-JD clear", "[DP-JD4]"},
		{"9UY clr", "[9UY4-H]"},
		{"J6 status", "[J6QB-P]"},
		{"UQ- clr?", "[UQ-PWD]"},
		{"U-Q hostile", "[U-QVWD]"},
		{"UQ alarm", "[UQ-PWD]"},

		{"S25C-K full of reds from UQ", "[S25C-K UQ-PWD]"},
		{"DP-JD on the IS-R gate", "[DP-JD4 IS-R7P]"},
		{"9UY ZQ Gate", "[9UY4-H ZQ-Z3Y]"},
		{"J6 and TA3 status", "[J6QB-P TA3T-3]"},
		{"UQ- and QSM clr?", "[QSM-LM UQ-PWD]"},
		{"U-Q to B-3 gate hostile", "[B-3QPD U-QVWD]"},
		{"UQ 36N alarm", "[36N-HZ UQ-PWD]"},

		{"Misha Sily  LGK-VP", "[]"},
	}

	mip := evemap.NewMap("Catch")

	syslist := mip.GetSystemList()

	for _, test := range tests {
		t.Run(test.message, func(t *testing.T) {
			result := ParseSystems(test.message, syslist, spyglasscfg.GetCfg().IntelCfg.IgnoreWords, 4)
			strkeys := []string{}
			for key := range result {

				//if value > 3 {
				//	t.Errorf("got value: %d, want <= 3", value)
				//}

				strkeys = append(strkeys, key)
			}

			sort.Strings(strkeys)

			if fmt.Sprintf("%v", strkeys) != test.fullnames {
				t.Errorf("got parse of %s, wanted %s", fmt.Sprintf("%v", strkeys), test.fullnames)
			}

		})
	}
}
