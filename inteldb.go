package eveintel

import (
	"sync"
	"time"
	"strings"
		"sort"
)

type inteldb struct {
	mu sync.Mutex

	messages imsgs
	raw_messages []string

	init bool
}

type imsgs []*IntelMessage

var database inteldb

func (msg imsgs) Len() int {
	return len(msg)
}

func (msg imsgs) Less(i, j int) bool {
	return msg[i].Time.Before(msg[j].Time)
}

func (msg imsgs) Swap(i, j int) () {
	msg[i], msg[j] = msg[j], msg[i]
}

func GetDatabase() (db *inteldb) {

	if !database.init {
		database = inteldb{init: true}
	}

	return &database
}

func (idb inteldb) GetAllIntel() (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	msgs = idb.messages

	return idb.messages
}

func (idb inteldb) GetStatusFiltered(status SystemState) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.State == status {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return
}

func (idb inteldb) GetSourceFiltered(source string) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.Source == source {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return
}

func (idb inteldb) GetSystemFiltered(sys string) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		for _, system := range msg.SysNames {
			if system == sys {
				msgs = append(msgs, msg)
			}
		}
	}

	sort.Sort(msgs)

	return
}

func (idb inteldb) GetChatroomFiltered(room string) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.Chatroom == room {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return
}

func (idb inteldb) GetReporterFilter(rep string) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.Reporter == rep {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return
}

func (idb inteldb) GetTimeFilter(early time.Time, late time.Time) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.Time.Before(late) && msg.Time.After(early) {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return msgs
}

func (idb inteldb) GetLatestFilter() (msgs imsgs){

	tn := time.Now().Add(1 * time.Second)

	tt := time.Now().Add(-25 * time.Minute)

	msgs = idb.GetTimeFilter(tn, tt)

	sort.Sort(msgs)

	return msgs
}


func (idb inteldb) GetReportWordFilter(word string) (msgs imsgs) {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if strings.Contains(msg.Report, word) {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return

}

func (idb inteldb) GetValid(valid bool) (msgs imsgs){

	idb.mu.Lock()
	defer idb.mu.Unlock()

	for _, msg := range idb.messages {
		if msg.Valid == valid {
			msgs = append(msgs, msg)
		}
	}

	sort.Sort(msgs)

	return

}
func (idb *inteldb)InsertIntelMessage(msg *IntelMessage) ()  {

	idb.mu.Lock()
	defer idb.mu.Unlock()

	match := false

	for _, msg2 := range idb.messages {
		if msg.WeakMatch(*msg2) == true {
			match = true
		}
	}

	if !match {
		idb.messages = append(idb.messages, msg)
	}
}

func (idb *inteldb) InsertRawMessage(msg string) () {
	idb.mu.Lock()
	defer idb.mu.Unlock()

	idb.raw_messages = append(idb.raw_messages, msg)
}

func (idb *inteldb) GetRawMessages () (messages []string){
	idb.mu.Lock()
	defer idb.mu.Unlock()

	return idb.raw_messages
}

func (idb *inteldb) DropParsedIntel() (){
	idb.mu.Lock()
	defer idb.mu.Unlock()

	idb.messages = imsgs{}
}

func (idb *inteldb) DropRawIntel() () {
	idb.mu.Lock()
	defer idb.mu.Unlock()

	idb.raw_messages = []string{}
}