package eveintel

import (
	"nanomsg.org/go-mangos"
	"nanomsg.org/go-mangos/protocol/sub"
	"log"
	"nanomsg.org/go-mangos/transport/tcp"
	"strings"
	"fmt"
	"github.com/renstrom/fuzzysearch/fuzzy"
	"sort"
	"time"
)

// IntelMessage encapsulates all the data around an intelligence report
type IntelMessage struct {
	SysNames []string
	State    SystemState
	Source   string
	Chatroom string
	Reporter string
	Time     time.Time
	Report   string
	Valid    bool
}


func (msg IntelMessage) String() string {

	return "[ " + strings.Join(msg.SysNames, ", ") + " ] --> " + msg.State.String() + " ; " + msg.Chatroom + " ; " + msg.Reporter + " ; " + msg.Report

}

// WeakMatch will return true if the chatroom, reporter and message are identical
func (msg IntelMessage) WeakMatch (msg2 IntelMessage) (match bool) {

	match = msg.Chatroom == msg2.Chatroom && msg.Reporter == msg2.Reporter && msg.Report == msg2.Report

	return match
}


// ListenForIntel is the main loop of the intel engine. It should be run in a goroutine.
// Prerequisites are that the the intel map has been initialised.
func (im *IntelEngine) ListenForIntel() () {

	var sock mangos.Socket
	var err error
	//var msg []byte

	dialAddr := fmt.Sprintf("tcp://127.0.0.1:13270")

	if sock, err = sub.NewSocket(); err != nil {
		log.Fatalf("can't get new sub socket: %v", err.Error())
	}
	sock.AddTransport(tcp.NewTransport())
	if err = sock.Dial(dialAddr); err != nil {
		log.Fatalf("can't dial on sub socket: %v", err.Error())
	}
	// Empty byte array effectively subscribes to everything
	err = sock.SetOption(mangos.OptionSubscribe, []byte(""))
	if err != nil {
		log.Fatalf("cannot subscribe: %v", err.Error())
	}

	go func() {

		defer log.Fatal("INTELPARSER LISTENING HAS FAILED")

		for {
			select {
			case <-im.quitListen:
				return
			default:
				log.Println("Waiting for message")
				msg, err2 := sock.Recv()
				log.Print("-RECV: ")
				log.Println(string(msg))
				im.messages <- string(msg)
				log.Println("Dispatched Message")
				if err2 != nil {
					log.Println("Could not receive nanomsg")
					log.Fatal(err2)
				}

			}
		}

	}()

	go func() {
		for {
			msg := <-im.messages

			intelmsg := ParseIntelMessage(msg, im.GetSystemList())
			if intelmsg.Valid {

				log.Println(intelmsg)

				im.idb.InsertIntelMessage(&intelmsg)
				im.idb.InsertRawMessage(msg)

				im.updateSystemStatus(intelmsg)

			}
		}
	}()




}

// ParseIntelMessage creates an intel imsgs based upon the content of a given message and the current intel map.
func ParseIntelMessage(msg string, systemlist []string) (message IntelMessage) {

	// 0 - Source
	// 1 - Chatroom
	// 2 - Message

	//fmt.Println(msg)

	mparts := strings.Split(msg, cfg.Delim)

	msg2 := mparts[2]

	//SysNames []string
	//State    SystemState
	//Source   string
	//Chatroom string
	//Reporter string
	//Time     time.Time
	//Report   string

	t, msg2 := stripTime(msg2)
	source := strings.ToLower(mparts[0])

	chatroom := mparts[1][0 : len(mparts[1])-20]

	validroom := false

	for _, v := range strings.Split(cfg.IntelChannels, ";") {
		if chatroom == v {
			validroom = true
		}
	}

	if validroom == false {
		message.Valid = false
		return
	}

	reporter, msg2 := stripSender(msg2)

	msg2 = strings.TrimSpace(msg2)

	sysnameall := ParseSystems(msg2, systemlist, strings.Split(cfg.IgnoreWords, ";"), cfg.MaximumScore)
	fmt.Println(sysnameall)
	sysnames := []string{}
	for k, _ := range sysnameall {
		sysnames = append(sysnames, k)
	}

	state := ParseStatus(msg2)
	report := msg2

	message = IntelMessage{SysNames: sysnames, State: state, Source: source, Chatroom: chatroom, Reporter: reporter, Time: t, Report: report, Valid: true}

	if len(message.SysNames) == 0 {
		message.State = UNK
	}

	fmt.Println(message)

	return message
}

// ParseStatus will check a message to see what type of message it has, returns the relevant SystemState.
func ParseStatus(msg string) (stat SystemState) {
	upper := strings.ToUpper(msg)

	for _, char := range strings.Split(cfg.IgnoreChars, ";") {
		upper = strings.Replace(upper, char, "", -1)
	}

	stat = ALM

	// Check if the system is clear
	for _, clr := range strings.Split(cfg.ClearWords, ";") {
		if strings.Contains(upper, clr) && !strings.HasSuffix(msg, "?") {
			stat = CLR
			return stat
		}
	}

	// Is someone requesting the status of the system?
	for _, st := range strings.Split(cfg.StatusWords, ";") {
		if strings.Contains(upper, st) {
			stat = REQ
			return stat
		}
	}

	if strings.HasSuffix(upper, "?") {
		stat = REQ
		return stat
	}

	// Check if the person has bad intel reporting methods
	for _, blu := range strings.Split(cfg.BlueWords, ";") {
		if strings.Contains(upper, blu) {
			stat = CLR
			return stat
		}
	}

	return

}

// ParseShips returns a list of shipnames found in the message (WIP)
// TODO: Actually implement ship parsing
func ParseShips(msg string, shiplist []string) (ships []string) {

	return
}

// ParseSystems will return a map of systems (if detected) and the confidence of that system.
// The confidence is the Levenshtein Distance of a word (potential system) to a known system name
func ParseSystems(msg string, systemlist []string, ignorewords []string, maxscore int) (systems map[string]int) {

	systems = make(map[string]int)

	upper := strings.ToUpper(msg)

	for _, system := range systemlist {
		sys := strings.ToUpper(system)

		// If its there in full..... Its definitely there
		if strings.Contains(upper, sys) {
			systems[sys] = 0
		}
	}

	words := strings.Split(upper, " ")

	wordsvalid := []string{}

	removeWord := func(a string, list []string) (bool) {
		for _, b := range list {
			if b == a {
				return true
			}
		}
		return false
	}

	for _, word := range words {
		if !removeWord(word, ignorewords) {
			wordsvalid = append(wordsvalid, word)
		}
	}

	for _, word := range wordsvalid {
		ranks := fuzzy.RankFindFold(word, systemlist)
		if len(ranks) == 1 {
			systems[ranks[0].Target] = ranks[0].Distance
		} else if len(ranks) > 1 {
			sort.Sort(ranks)

			//If we have some tests with the same score then we need to decide which one to use
			// First lets get all the words with the same score
			score := ranks[0].Distance

			rut := []fuzzy.Rank{}

			for _, rank := range ranks {
				if rank.Distance == score {
					rut = append(rut, rank)
				}
			}

			// Do we have a match that is a prefix

			prefs := []fuzzy.Rank{}

			for _, rank := range rut {
				if strings.HasPrefix(rank.Target, rank.Source) {
					// Substring is usually a good hint lol....
					prefs = append(prefs, rank)
				}
			}

			if len(prefs) == 0 {
				// Well we dont have a direct prefix, lets try removing dashes and see how we go

				for _, rank := range rut {
					if strings.HasPrefix(rank.Target, strings.Replace(rank.Source, "-", "", -1)) {
						prefs = append(prefs, rank)
					}
				}
			}

			if len(prefs) == 1 {
				//yay this is our system :D
				systems[prefs[0].Target] = prefs[0].Distance
				continue
			} else if len(prefs) == 0 {
				// We have the case where there is no direct prefix, lets check for a simpler Contains

				subs := []fuzzy.Rank{}

				for _, rank := range rut {
					if strings.Contains(rank.Target, rank.Source) {
						// So we have a contains match but not a direct prefix
						// Add it to subs and we will see how many we end up with....
						subs = append(subs, rank)
					}
				}

				// See how many substring matches we have

				if len(subs) == 0 {
					// So we have no prefix and no substring matches.... Lets alarm all fuzzy matches just to be sure...
					// Better to alarm than not

					for _, rank := range rut {
						systems[rank.Target] = rank.Distance
					}
					continue
				} else if len(subs) == 1 {
					// Well we narrowed it down to 1 match, so lets just alarm that one....
					systems[subs[0].Target] = subs[0].Distance
					continue
				} else {
					// Reaching here means we still have multiple systems matching....
					// People really need to learn how to report intel....
					// To potentially save their asses I will report all matching systems
					// But seriously, how hard is it to drag a system name from the top left corner of the screen.....

					for _, rank := range subs {
						systems[rank.Target] = rank.Distance
					}
					continue
				}

			}

			systems[ranks[0].Target] = ranks[0].Distance
		}

		for val, key := range systems {
			if key > maxscore {
				delete(systems, val)
			}
		}
	}

	return

}

// stripTime for a given msg (string) will parse the time from the log (given its a line from a CCP chatlog)
// It returns the ime opbject and the message without the timestamp
func stripTime(msg string) (t time.Time, message string) {

	format := "2006.01.02 15:04:05"

	timestr := msg[2:21]
	message = msg[24:]

	t, _ = time.Parse(format, timestr)

	return
}

// stripSender for a given message will parse the sender of the message (given its a line from CCP chatlog (less timestamp)
// It returns the sender and the message string lest the senders name and delimiter
func stripSender(msg string) (sender string, message string) {

	index := strings.Index(msg, " > ")

	message = msg[index+3:]
	sender = msg[:index]

	return
}
