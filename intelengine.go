package eveintel

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// IntelEngine is the base object of the intel engine.
// It contains the system list and all (runtime) persistent data
type IntelEngine struct {
	// Maps system names to a number indicating state
	//
	SystemMap map[string]SystemStatus

	currentRegion string

	idb *inteldb

	quitListen chan bool
	messages   chan string

	init bool
}

type SpyglassConfig struct {
	// Delimeter used in messages
	Delim string `json:"delim"`

	// The aggressiveness of word matching in intel searches
	MaximumScore int `json:"maximum_score"`

	// Characters/Words in intel parsing, comma seperated
	IgnoreChars string `json:"ignore_chars"`
	ClearWords  string `json:"clear_words"`
	BlueWords   string `json:"blue_words"`
	StatusWords string `json:"status_words"`
	IgnoreWords string `json:"ignore_words"`

	// Which intel channels to monitor, comma seperated
	IntelChannels string `json:"intel_channels"`

	// Base URL to get to maps from
	BaseURL string `json:"base_url"`

	// Jumpbridge URL or local filepath
	JumpbridgeUrl string `json:"jumpbridge_url"`

	//Path to the eve chatlogs
	LogPath string `json:"log_path"`

	//The Current Region
	Region     string `json:"region"`
	ConfigPath string `json:"config_path"`
}

var (
	cfg          *SpyglassConfig
	intelligence *IntelEngine
)

const (
	port = 13271
)

// Universe is the format that we can decode from the json evemap server
type Universe struct {
	// Name is the region name
	Name string
	// Systems is exactly that, a list of systems
	Systems []System
	// Jumps lists all the connections between systems
	Jumps []Stargate
	// MapFactor scales the map to size, Only kept for compat with evemap version
	MapFactor float64
}

type System struct {
	// Name is the name of the solar system
	Name string
	// ID is the CCP ID for the system
	ID float64

	// CenterX is the position on the svg map X coord
	CenterX float64
	// CenterY is the position on the svg map Y coord
	CenterY float64

	// Neighbours is a list of all neighbouring solar systems
	Neighbours []Neighbour

	Status SystemStatus
}

type Neighbour struct {
	ID   float64
	Name string
}

type Stargate struct {
	S1 Neighbour
	S2 Neighbour

	x1 float64
	y1 float64

	x2 float64
	y2 float64

	//Reserved for future use
	Class string
}

type SystemStatus struct {
	State  SystemState
	Update time.Time
}

// SystemState enumerates the possible states of a system
type SystemState uint8

const (
	// UNK --> Unknown status
	UNK SystemState = 0
	// CLR --> System is clear
	CLR SystemState = 1
	// REQ --> Someone is requesting the system status
	REQ SystemState = 2
	// ALM -- > System is hostile by last report
	ALM SystemState = 3 // Alarm
)

// String will return an english representation of SystemState
func (state SystemState) String() string {
	names := [...]string{
		"unknown",
		"clear",
		"request",
		"alarm"}

	if state < UNK || state > ALM {
		return "unknown"
	}

	return names[state]
}

func (status SystemStatus) String() string {
	return status.State.String()
}

func StartIntelEngine() (engine *IntelEngine) {

	msgs := make(chan string, 100)
	intelligence = &IntelEngine{init: false, idb: GetDatabase(), messages: msgs}
	fetchConfig()

	region := cfg.Region

	intelligence.initIntelRegion(region)
	intelligence.ListenForIntel()

	return intelligence

}

func fetchConfig() {
	cfgURL := "http://127.0.0.1:13273/config/get"

	client := http.Client{Timeout: 10 * time.Second}

	req, err := http.NewRequest(http.MethodGet, cfgURL, nil)
	if err != nil {
		panic(err)
	}

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	cfg = &SpyglassConfig{}

	err = json.Unmarshal(body, &cfg)
	if err != nil {
		panic(err)
	}

}

func (engine *IntelEngine) initIntelRegion(region string) () {

	if region == "" {
		panic("Empty Region")
	}

	engine.currentRegion = ""
	engine.SystemMap = nil

	mengport := 13272

	url := fmt.Sprintf("http://127.0.0.1:%v/region/systems/%s", mengport, region)

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Println(err)
		log.Fatal("Failed to get systemlist from evemap... Is it started?")
	}

	client := &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		log.Fatal("Failed to get systemlist from evemap.... Is it started?")
	}

	defer resp.Body.Close()

	regionMap := Universe{}

	if err := json.NewDecoder(resp.Body).Decode(&regionMap); err != nil {
		log.Println(err)
		log.Fatal("Error decoding systemlist. Contact the developer..... He fucked up")
	}

	engine.SystemMap = make(map[string]SystemStatus)

	for _, sys := range regionMap.Systems {
		engine.SystemMap[sys.Name] = SystemStatus{UNK, time.Now().Add(-24 * time.Hour)}
		log.Println(sys.Name)
	}

	engine.currentRegion = region

	engine.idb.DropParsedIntel()

	if len(engine.idb.GetRawMessages()) > 0 {
		for _, msg := range engine.idb.GetRawMessages() {
			imsg := ParseIntelMessage(msg, engine.GetSystemList())
			engine.idb.InsertIntelMessage(&imsg)
			engine.updateSystemStatus(imsg)
		}
	}

	log.Println("Updated intel region to - " + engine.currentRegion)
}

func (im *IntelEngine) updateSystemStatus(msg IntelMessage) {
	sysStatus := SystemStatus{State:msg.State, Update:msg.Time}
	for _, sys := range msg.SysNames {
		if sys == "" {
			continue
		}
		im.SystemMap[sys] = sysStatus
	}
}

func (engine *IntelEngine) GetSystemList() (list []string) {

	list = make([]string, 1)

	for sys, _ := range engine.SystemMap {
		list = append(list, sys)
	}

	return list
}

func getEveMap(region string) (mip Universe) {

	mengport := 13272

	url := fmt.Sprintf("http://127.0.0.1:%v/region/systems/%s", mengport, region)

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Println(err)
		panic("Failed to get systemlist from evemap... Is it started?")
	}

	client := &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		panic("Failed to get systemlist from evemap... Is it started?")
	}

	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&mip); err != nil {
		log.Println(err)
		panic("Error decoding systemlist. Contact the developer..... He fucked up")
	}

	return

}

func getSystemList() (maps []string) {

	mengport := 13272

	url := fmt.Sprintf("http://127.0.0.1:%v/regions", mengport)

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Println(err)
		log.Fatal("Failed to get region from evemap... Is it started?")
	}

	client := &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		log.Fatal("Failed to get region from evemap... Is it started?")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	mip := make(map[string]string)

	log.Println(string(body))

	if err := json.Unmarshal(body, &mip); err != nil {
		log.Println(err)
		log.Fatal("Error decoding regionlist. Contact the developer..... He fucked up")
	}

	maps = make([]string, 1)

	for key, _ := range mip {
		maps = append(maps, key)
	}

	return

}
